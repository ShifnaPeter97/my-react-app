import { BrowserRouter, Route, Routes } from 'react-router-dom';
import './App.css';
import Account from './Components/Account';
import Brands from './Components/Brands';
import Files from './Components/Files';
import Header from './Components/Header';
import Home from './Components/Home';
import Projects from './Components/Projects';
import SideMenu from './Components/SideMenu';
import TeamConnect from './Components/TeamConnect';

function App() {
  return (
    <div className="App">
      <SideMenu />
      <Header />
      {/* <BrowserRouter>
        <Routes>
          <Route exact path='/' element={<Home />} />
          <Route path='/projects' element={<Projects />} />
          <Route path='/brands' element={<Brands />} />
          <Route path='/files' element={<Files />} />
          <Route path='/team-connect' element={<TeamConnect />} />
          <Route path='/account' element={<Account />} />
          <Route path='/logout' element={<Home />} />
        </Routes>
      </BrowserRouter> */}
    </div>
  );
}

export default App;
