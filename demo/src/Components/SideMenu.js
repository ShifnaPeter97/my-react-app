import React, { useState } from 'react'
import logo from '../Assets/LOGO-SHADOW.png'
import { SidebarData } from './SidebarData';

function SideMenu() {
    const [inactive, setInactive] = useState(false);
    return (
        <div className={`side-menu ${inactive ? 'inactive' : ""}`}>
            <div className="sidebar-header">
                <div className="top-section">
                    <div className="logo">
                        <img src={logo} alt="Trust Me" />
                    </div>
                    <div onClick={() => setInactive(!inactive)} className="toggle-menu-btn">
                        <i class="bi bi-list"></i>
                    </div>
                </div>
                <div className="search-controller">
                    <button className="search-btn">
                        <i class="bi bi-search"></i>
                    </button>
                    <input type="text" placeholder='Search' />
                </div>
                <div className="divider"></div>
            </div>
            <ul className="sidebarList">
                {SidebarData.map((val, key) => {
                    return (
                        <li
                            key={key}
                            id={window.location.pathname == val.link ? "active" : ""}
                            className="row"
                            onClick={() => {
                                window.location.pathname = val.link
                            }}
                        >
                            <div id="icon">
                                {val.icon}
                            </div>
                            <div id="title">
                                {val.title}
                            </div>
                        </li>
                    )
                })}
            </ul>
        </div>
    )
}

export default SideMenu