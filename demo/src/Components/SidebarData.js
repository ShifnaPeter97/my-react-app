import React from 'react'
import AccountTreeIcon from '@mui/icons-material/AccountTree';
import BrandingWatermarkIcon from '@mui/icons-material/BrandingWatermark';
import InsertDriveFileIcon from '@mui/icons-material/InsertDriveFile';
import GroupsIcon from '@mui/icons-material/Groups';
import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import LogoutIcon from '@mui/icons-material/Logout';

export const SidebarData=[
    {
        title: "Projects",
        icon: <AccountTreeIcon />,
        link: "/projects"
    },
    {
        title: "Brands",
        icon: <BrandingWatermarkIcon />,
        link: "/brands"
    },
    {
        title: "Files",
        icon: <InsertDriveFileIcon />,
        link: "/files"
    },
    {
        title: "Team Connect",
        icon: <GroupsIcon />,
        link: "/team-connect"
    },
    {
        title: "Account",
        icon: <AccountCircleIcon />,
        link: "/account"
    },
    {
        title: "Logout",
        icon: <LogoutIcon />,
        link: "/logout"
    }
]
