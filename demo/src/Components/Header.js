import { Dining } from '@mui/icons-material';
import React from 'react'
import { SidebarData } from './SidebarData'

function Header() {
    let data = "";
    return (
        <div className='inner-content'>
            {SidebarData.map((val, key) => {
                data = window.location.pathname == val.link ? val.title : "";
                return data;
            })}
        </div>
    )
}

export default Header